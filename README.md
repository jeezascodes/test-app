## Instructions

After cloning this project you'll need to run:

### `cd test-app`

### `npm install`

### `npm start`

## Documentation

### Overview:

This project was created using React with Hooks.

The basic structure goes like this:

- A stateless component which only renders the table
- A container where all the logic is located, this container imports the TableComponent and passes the data as props to its children component
- And finally a generic app.js document where the entire app gets rendered

### How is data processed and formatted

All data provided for this test can be found in the "constans.js" file inside the components folder

1. Frist, data is imported from the constans.js file into the container
2. Then data from both jsons gets formatted into a single array of objects called `formattedData`
3. Each object item calls the function `getCellTotal` which essentialy sums all the values on that cell for the available time period
4. Then i created an object called `totals` where the totals for every columns are obtained by using the `getTotal` function which sums the values of the entire column unsing the the `key` parameter to identigy the column
5. Some values are averges or percentages, in both cases these values are obtained by using the `getAvergae` and `getPercentage` functions respectively
6. After all the totals are obtained, they the pushed to the `formattedData` array using the `push()` method
7. Next, a new array called `averages` is created where all the averages are obtained using the `getAvergae` function and the `totalDays`variable, this is the data that is shown when the user selects the "Mostrat promedios" button
8. the `totalDays` varible is actually an React state, and its set during the execution of the `useEffect` hook, it counts the length of an array generated with all the days available on the data previously provided
9. Then, in order to hide columns where all the values are 0, i use `getEmptyCells` in the `useEffect` hook, this filters any cell in the `totals` array which value is equal to 0 and sets the `emptyCols` state with the resulting indeces
10. The indices stored in the `emptyCols` state are then used to validate is a column should be rendered or not
11. Finally the accompanying text to every cell gets set by a couple of functions: first `mapColumnsToText` functions maps titles to its indices and text and then the `mapIndexToText` receives and index and compares it to the respective index returned by the `mapIndexToText` to then return the correct string.
