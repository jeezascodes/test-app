import React from "react";
import "./TableComponent.css";

const TableComponent = ({
  setShowAverage,
  mapIndexToText,
  showAverage,
  filterdTitles,
  filterdData,
  filterdAverages
}) => {
  return (
    <div>
      <div>
        <div className="header">
          Mostrar Promedios
          <button
            className="checkbox"
            onClick={() => setShowAverage(!showAverage)}
          >
            {showAverage && (
              <img className="svg" src={process.env.PUBLIC_URL + "/tick.svg"} />
            )}
          </button>
        </div>
        <table>
          <thead>
            <tr>
              {filterdTitles.map(item => (
                <th>{item}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {!showAverage
              ? filterdData.map(data => {
                  const cells = Object.values(data);
                  return (
                    <tr>
                      {cells.map((cell, index) => (
                        <td>
                          {cell} {mapIndexToText(index)}
                        </td>
                      ))}
                    </tr>
                  );
                })
              : filterdAverages.map(data => {
                  const cells = Object.values(data);
                  return (
                    <tr>
                      {cells.map((cell, index) => (
                        <td>
                          {cell} {mapIndexToText(index)}
                        </td>
                      ))}
                    </tr>
                  );
                })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default TableComponent;
