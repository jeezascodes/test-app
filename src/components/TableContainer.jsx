import React, { useState, useEffect } from "react";
import { tableColumns, brandDateDate, assignedStore } from "./constans";
import TableComponent from "./TableComponent";
import "./TableComponent.css";

const TableContainer = () => {
  const [showAverage, setShowAverage] = useState(false);
  /* The indices stored in the `emptyCols` state are then used to validate is a column should be rendered or not */
  const [emptyCols, setEmptyCols] = useState(null);
  const [totalDays, setTotalDays] = useState(null);
  const stores = assignedStore.data;
  const reducer = (accumulator, currentValue) => accumulator + currentValue;

  useEffect(() => {
    const getDays = brandDateDate.map(brand => {
      const totalDays = Object.values(brand.tickets).length;
      return totalDays;
    });

    /* this filters any cell in the `totals` array which value is equal to 0 and sets the `emptyCols` state with the resulting indeces */
    const getEmptyCells = Object.values(totals).filter((val, index) => {
      if (val === 0) setEmptyCols(index);
    });

    setTotalDays(getDays.reduce(reducer) / getDays.length);
    mapColumnsToText();
  }, []);

  /* getCellTotal essentialy sums all the values on that cell for the available time period */
  const getCellTotal = data => {
    const total = Number(
      Object.values(data)
        .reduce(reducer)
        .toFixed(2)
    );
    return total;
  };

  const getPercentage = (value, whole) => {
    const percentage = Number(((value * 100) / whole).toFixed(2));
    return percentage;
  };

  const getAvergae = (value, whole) => Number((value / whole).toFixed(2));

  /* getTotal function sums the values of the entire column unsing the the `key` parameter to identigy the column */
  const getTotal = (data, key, isAverage) => {
    const total = data.map(a => a[key]).reduce(reducer);
    const formatted = isAverage
      ? Number((total / data.length).toFixed(2))
      : Number(total.toFixed(2));
    return formatted;
  };

  /* `mapColumnsToText` functions maps titles to its indices and text */
  const mapColumnsToText = () => {
    const mappingg = filterdTitles.map((title, index) => {
      if (title === "Atracción") return { id: index, text: "%" };
      if (title === "Conversión de compra") return { id: index, text: "%" };
      if (title === "Permanencia promedio") return { id: index, text: "min" };
      if (title === "Días apagados") return { id: index, text: "Días" };
      return null;
    });
    const filterdIndex = mappingg.filter(f => f !== null);
    return filterdIndex;
  };

  /* `mapIndexToText` receives and index and compares it to the respective index returned by the `mapIndexToText` to then return the correct string. */
  const mapIndexToText = index => {
    const allIndex = mapColumnsToText();
    const text = allIndex.map(a => {
      if (index === a.id) return a.text;
    });
    return text;
  };

  /* Data from both jsons gets formatted into a single array called formattedData*/
  const formattedData = stores.map(store => {
    const found = brandDateDate.find(
      element => element.identifier === store.identifier
    );
    const turnedOffDays = Object.values(found.uptime).filter(u => u === 0);

    const data = {
      title: store.name,
      peasants: getCellTotal(found.peasants),
      visitors: getCellTotal(found.visitors),
      attraction: getPercentage(
        getCellTotal(found.visitors),
        getCellTotal(found.peasants)
      ),
      cabinet: getCellTotal(found.cabinet),
      tickets: getCellTotal(found.tickets),
      persuasion: getPercentage(
        getCellTotal(found.tickets),
        getCellTotal(found.visitors)
      ),
      revenue: getCellTotal(found.revenue),
      averageTicket: getAvergae(
        getCellTotal(found.revenue),
        getCellTotal(found.tickets)
      ),
      items: getCellTotal(found.items),
      itemperTicket: getAvergae(
        getCellTotal(found.items),
        getCellTotal(found.tickets)
      ),
      averagePermanence: Number(
        (
          getPercentage(
            getCellTotal(found.permanence),
            getCellTotal(found.permanenceCount)
          ) / 6000000
        ).toFixed(2)
      ),
      turnedOffDays: turnedOffDays.length
    };
    return data;
  });

  /* totals where the totals for every columns are obtained and stored */
  const totals = {
    title: "Totales",
    peasants: getTotal(formattedData, "peasants"),
    visitors: getTotal(formattedData, "visitors"),
    attraction: getTotal(formattedData, "attraction", true),
    cabinet: getTotal(formattedData, "cabinet"),
    tickets: getTotal(formattedData, "tickets"),
    persuasion: getTotal(formattedData, "persuasion", true),
    revenue: getTotal(formattedData, "revenue"),
    averageTicket: getTotal(formattedData, "averageTicket"),
    items: getTotal(formattedData, "items"),
    itemperTicket: getTotal(formattedData, "itemperTicket", true),
    averagePermanence: getTotal(formattedData, "averagePermanence", true),
    turnedOffDays: getTotal(formattedData, "turnedOffDays")
  };

  /* totals gets pushed to formattedData */
  const withTotals = formattedData.push(totals);

  /* this is the data that is shown when the user selects the "Mostrat promedios" button */
  const averages = formattedData.map(data => {
    const average = {
      title: data.title,
      peasants: getAvergae(data.peasants, totalDays),
      visitors: getAvergae(data.visitors, totalDays),
      attraction: data.attraction,
      cabinet: getAvergae(data.cabinet, totalDays),
      tickets: getAvergae(data.tickets, totalDays),
      persuasion: data.persuasion,
      revenue: getAvergae(data.revenue, totalDays),
      averageTicket: data.averageTicket,
      items: getAvergae(data.items, totalDays),
      itemperTicket: data.itemperTicket,
      averagePermanence: data.averagePermanence,
      turnedOffDays: data.turnedOffDays
    };
    return average;
  });

  const averageValues = averages.map(av => Object.values(av));
  const filterdAvergaes = averageValues.map(v =>
    v.filter((f, index) => index !== emptyCols)
  );
  const formattedValues = formattedData.map(av => Object.values(av));
  const filterdData = formattedValues.map(v =>
    v.filter((f, index) => index !== emptyCols)
  );
  const filterdTitles = tableColumns.filter((f, index) => index !== emptyCols);

  return (
    <TableComponent
      setShowAverage={setShowAverage}
      mapIndexToText={mapIndexToText}
      showAverage={showAverage}
      filterdTitles={filterdTitles}
      filterdData={filterdData}
      filterdAverages={filterdAvergaes}
    />
  );
};

export default TableContainer;
